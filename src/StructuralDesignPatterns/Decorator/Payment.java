package StructuralDesignPatterns.Decorator;

public interface Payment {
    void operation();
    double getCommission();

}
