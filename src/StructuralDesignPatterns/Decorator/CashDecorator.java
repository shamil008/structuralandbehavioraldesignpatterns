package StructuralDesignPatterns.Decorator;

public abstract class CashDecorator implements Payment{
    protected Payment payment;

    public CashDecorator(Payment payment) {
        this.payment = payment;
    }

    @Override
    public void operation() {

    }

    @Override
    public double getCommission() {
        return payment.getCommission();
    }
}
