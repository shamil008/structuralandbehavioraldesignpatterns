package StructuralDesignPatterns.Decorator;

public class Operation {
    public static void main(String[] args) {
        Payment payment = new CashPayment();
        payment.operation();
        System.out.println("Commission = "+payment.getCommission());
        payment = new CardDecorator(payment);
        System.out.println("Commission = "+payment.getCommission());
    }
}