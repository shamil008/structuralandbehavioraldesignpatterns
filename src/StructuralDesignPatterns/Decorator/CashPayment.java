package StructuralDesignPatterns.Decorator;

public class CashPayment implements Payment{
    @Override
    public void operation() {
        System.out.println("Successful payment!");
    }

    @Override
    public double getCommission() {
        return 0;
    }
}
