package StructuralDesignPatterns.Decorator;

public class CardDecorator extends CashDecorator{
    public CardDecorator(Payment payment) {
        super(payment);
    }
    @Override
    public double getCommission() {
        return payment.getCommission()+0.5;
    }
}
