package StructuralDesignPatterns.Bridge;

public class ChampionshipCreator {
    public static void main(String[] args) {

        Championship [] championships = {
                new EuroCup(new Football()),
                new WorldCup(new Basketball())
        };
        for(Championship championship : championships){
            championship.playCup();

        }
    }
}