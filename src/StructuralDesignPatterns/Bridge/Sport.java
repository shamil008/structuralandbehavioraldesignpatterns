package StructuralDesignPatterns.Bridge;

public interface Sport {
    void play();
}
