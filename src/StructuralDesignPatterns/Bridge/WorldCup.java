package StructuralDesignPatterns.Bridge;

public class WorldCup extends Championship {
    public WorldCup(Sport sport) {
        super(sport);
    }

    @Override
    public void playCup() {
        System.out.println("World cup is starting");
        getSport().play();

    }
}
