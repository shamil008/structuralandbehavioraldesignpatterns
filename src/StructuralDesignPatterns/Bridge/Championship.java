package StructuralDesignPatterns.Bridge;

public abstract class Championship {
    private Sport sport;

    public Championship(Sport sport) {
        this.sport = sport;
    }
    public abstract void playCup();
    public Sport getSport(){
        return sport;
    }

}
