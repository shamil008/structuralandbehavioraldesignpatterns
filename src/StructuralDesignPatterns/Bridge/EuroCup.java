package StructuralDesignPatterns.Bridge;

public class EuroCup extends Championship {
    public EuroCup(Sport sport) {
        super(sport);
    }

    @Override
    public void playCup() {
        System.out.println("Euro cup is starting");
        getSport().play();

    }
}
