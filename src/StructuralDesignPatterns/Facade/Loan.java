package StructuralDesignPatterns.Facade;

public class Loan {
    public void applyForLoan(String accountNumber,Double amount){
        System.out.println("Applying for loan of " + amount + " for account " + accountNumber);
    }
}
