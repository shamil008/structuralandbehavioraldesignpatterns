package StructuralDesignPatterns.Facade;

public class Account {
    private String accountNumber;

    public Account(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public void checkBalance(){
        System.out.println("Check balance for account "+accountNumber);
    }
    public void deposit(double amount){
        System.out.println(amount + " deposited into account " + accountNumber);
    }

    public String getAccountNumber() {
        return accountNumber;
    }
}
