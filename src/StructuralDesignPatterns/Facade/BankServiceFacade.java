package StructuralDesignPatterns.Facade;

public class BankServiceFacade {
    private Account account;
    private Customer customer;
    private Loan loan;

    public BankServiceFacade(String accountNumber, String customerName, Long customerId) {
        this.account = new Account(accountNumber);
        this.customer = new Customer(customerId,customerName);
        this.loan = new Loan();
    }
    public void deposit(double amount){
        account.deposit(amount);
    }
    public void checkBalance(){
        account.checkBalance();
    }
    public void getCustomerDetails(){
        customer.getCustomerDetails();
    }
    public void applyForLoan(double amount){
        loan.applyForLoan(account.getAccountNumber(), amount);
    }

}
