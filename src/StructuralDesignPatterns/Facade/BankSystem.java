package StructuralDesignPatterns.Facade;

public class BankSystem {
    public static void main(String[] args) {
        BankServiceFacade bankService= new BankServiceFacade("C57674","Shamil Vasanov",1l);
        bankService.deposit(1000);
        System.out.println();

        bankService.checkBalance();
        System.out.println();

        bankService.getCustomerDetails();
        System.out.println();

        bankService.applyForLoan(10000);

    }
}