package StructuralDesignPatterns.Facade;

public class Customer {
    private long customerId;
    private String customerName;

    public Customer(long customerId, String customerName) {
        this.customerId = customerId;
        this.customerName = customerName;
    }
    public void getCustomerDetails(){
        System.out.println("Customer Id: "+customerId+" Customer name "+customerName);
    }
}

