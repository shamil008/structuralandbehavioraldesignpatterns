package StructuralDesignPatterns.Proxy;

public class ProxyGame implements Game{
    private String gameName;

    public ProxyGame(String gameName) {
        this.gameName = gameName;
    }

    private RealGame realGame;
    @Override
    public void play() {
        if (realGame == null){
            realGame = new RealGame(gameName);
        }
        realGame.play();

    }
}
