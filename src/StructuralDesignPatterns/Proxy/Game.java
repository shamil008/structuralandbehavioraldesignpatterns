package StructuralDesignPatterns.Proxy;

public interface Game {
    void play();
}
