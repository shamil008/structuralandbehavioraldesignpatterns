package StructuralDesignPatterns.Proxy;

public class Main {
    public static void main(String[] args) {

       var proxyGame = new ProxyGame("FC24");
       proxyGame.play();
        System.out.println("");
        proxyGame.play();

    }
}