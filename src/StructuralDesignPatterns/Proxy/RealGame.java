package StructuralDesignPatterns.Proxy;

public class RealGame implements Game{
    private String gameName;

    public RealGame(String gameName) {
        this.gameName = gameName;
        downloadGame();
    }

    @Override
    public void play() {
        System.out.println(gameName+" game playing");

    }
    public void downloadGame(){
        System.out.println(gameName+" game downloading...");
    }
}
