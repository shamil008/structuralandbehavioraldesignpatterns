package BehavioralDesignPatterns.Strategy;

public class MarketOrder implements  OrderStrategy{
    private String order;
    private int count;
    private double price;

    public MarketOrder(String order, int count, double price) {
        this.order = order;
        this.count = count;
        this.price = price;
        order(order);
    }

    @Override
    public void order(String orderType) {
        System.out.println(orderType +" ordered market");
    }
}
