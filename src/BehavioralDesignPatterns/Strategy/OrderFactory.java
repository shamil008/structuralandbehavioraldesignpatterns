package BehavioralDesignPatterns.Strategy;

public class OrderFactory {

    public OrderStrategy getOrderStrategy(Places places){
        return switch (places){
            case MARKET ->  new MarketOrder("Fuse tea",2,1.2);
            case RESTAURANT ->  new RestaurantOrder("Doner","Ayran",2,3.5);
        };
    }
}
