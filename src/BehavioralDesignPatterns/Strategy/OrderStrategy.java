package BehavioralDesignPatterns.Strategy;

public interface OrderStrategy {
    void order(String orderType);
}
