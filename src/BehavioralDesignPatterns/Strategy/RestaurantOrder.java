package BehavioralDesignPatterns.Strategy;

public class RestaurantOrder implements  OrderStrategy{
    private  String food;
    private String drink;
    private int count;
    private  double price;

    public RestaurantOrder(String food, String drink, int count, double price) {
        this.food = food;
        this.drink = drink;
        this.count = count;
        this.price = price;
        order(food);
    }

    @Override
    public void order(String orderType) {
        System.out.println(orderType+" ordered restaurant");
    }
}
