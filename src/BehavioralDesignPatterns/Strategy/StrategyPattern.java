package BehavioralDesignPatterns.Strategy;

public class StrategyPattern {
    public static void main(String[] args) {

        var orderFactory = new OrderFactory();
        orderFactory.getOrderStrategy(Places.RESTAURANT);
    }
}