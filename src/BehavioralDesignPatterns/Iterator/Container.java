package BehavioralDesignPatterns.Iterator;

public interface Container {
    Iterator getIterator();
}
