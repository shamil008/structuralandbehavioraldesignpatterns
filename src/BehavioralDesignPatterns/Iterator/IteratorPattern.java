package BehavioralDesignPatterns.Iterator;

public class IteratorPattern {
    public static void main(String[] args) {

        var programmingLanguageRepository = new ProgrammingLanguagesRepository();
        for(Iterator iter = programmingLanguageRepository.getIterator();iter.hasNext();){
            var programmingLanguage = (String) iter.next();
            System.out.println("Programming Language : "+programmingLanguage);

        }
    }
}