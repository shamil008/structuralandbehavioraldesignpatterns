package BehavioralDesignPatterns.Iterator;

public class ProgrammingLanguagesRepository implements Container{
    private String [] programmingLanguages = {"Java","Kotlin","C#","Python","Go"};
    @Override
    public Iterator getIterator() {
        return new ProgrammingLanguagesIterator(programmingLanguages);
    }
}
