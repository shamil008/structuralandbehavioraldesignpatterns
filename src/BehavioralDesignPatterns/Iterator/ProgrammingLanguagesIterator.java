package BehavioralDesignPatterns.Iterator;

public class ProgrammingLanguagesIterator implements  Iterator{
    private String[] programmingLanguages;
    private int index;

    public ProgrammingLanguagesIterator(String[] programmingLanguages) {
        this.programmingLanguages = programmingLanguages;
    }

    @Override
    public boolean hasNext() {
        return index< programmingLanguages.length;
    }

    @Override
    public Object next() {
        if(this.hasNext()){
            return programmingLanguages[index++];
        }
        return null;
    }
}
