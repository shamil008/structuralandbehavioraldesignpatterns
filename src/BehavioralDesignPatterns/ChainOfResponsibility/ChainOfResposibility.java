package BehavioralDesignPatterns.ChainOfResponsibility;

public class ChainOfResposibility {
    public static void main(String[] args) {
        var cardPayment = new CardPayment();
        var cashPayment = new CashPayment();

        cardPayment.setNextHandler(cashPayment);

        var payment1 = new Payment(105);
        var payment2 = new Payment(95);

        cardPayment.handlePayment(payment1);
        cardPayment.handlePayment(payment2);
    }
}