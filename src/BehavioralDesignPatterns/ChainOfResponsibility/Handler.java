package BehavioralDesignPatterns.ChainOfResponsibility;

public interface Handler {
    void handlePayment(Payment payment);
}
