package BehavioralDesignPatterns.ChainOfResponsibility;

public class CardPayment implements Handler{
    private Handler nextHandler;

    @Override
    public void handlePayment(Payment payment) {
        if(payment.getAmount()>=100){
            System.out.println("Uğurlu ödəniş");
        } else if (nextHandler != null) {
            nextHandler.handlePayment(payment);
            
        }
        else{
            System.out.println("Balansda yetərli qədər məbləğ yoxdur!");
        }
    }

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }
}
