package BehavioralDesignPatterns.ChainOfResponsibility;

public class CashPayment implements Handler{
    @Override
    public void handlePayment(Payment payment) {
        System.out.println("Nəğd ödəniş edildi");
    }
}
