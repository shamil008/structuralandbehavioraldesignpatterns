package BehavioralDesignPatterns.Observer;

public class ObserverPattern {
    public static void main(String[] args) {

        var instagram = new Instagram();
        var user1 = new User("User 1");
        var user2 = new User("User 2");
        var user3 = new User("User 3");

        instagram.follow(user1);
        instagram.follow(user2);
        instagram.follow(user3);

        instagram.shareNewPost("GS 0-1 FB");

        instagram.unfollow(user1);

        instagram.shareNewPost("Gooooool");
    }
}