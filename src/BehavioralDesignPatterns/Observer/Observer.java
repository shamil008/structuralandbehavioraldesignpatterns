package BehavioralDesignPatterns.Observer;

public interface Observer {
    void update(String post);
}
