package BehavioralDesignPatterns.Observer;

import java.util.ArrayList;
import java.util.List;

public class Instagram implements Subject {
    private List<Observer> observers = new ArrayList<>();
    private String newPost;

    @Override
    public void follow(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unfollow(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void sendNotification() {
        for (Observer observer :observers){
            observer.update(newPost);
        }

    }
    public void shareNewPost(String newPost){
        this.newPost = newPost;
        sendNotification();

    }

}
