package BehavioralDesignPatterns.Observer;

public interface Subject {
    void follow(Observer observer);
    void unfollow(Observer observer);
    void sendNotification();

}
