package BehavioralDesignPatterns.Observer;

public class User implements Observer{
    private String userName;

    public User(String userName) {
        this.userName = userName;
    }

    @Override
    public void update(String post) {
        System.out.println(userName + " liked new post " + post);

    }
}
